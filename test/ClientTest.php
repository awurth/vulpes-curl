<?php

namespace Test\Vulpes\cURL;

use PHPUnit\Framework\TestCase;
use Vulpes\cURL\Client;
use Vulpes\cURL\Exception\RuntimeException;
use Vulpes\cURL\Exception\cURLError;
use Vulpes\cURL\Info;
use Vulpes\cURL\Options;
use Vulpes\cURL\Session;
use Vulpes\cURL\SessionInterface;

class ClientTest extends TestCase
{
    private const DEFAULT_RESPONSE = 'Vulpes';

    public function clientProvider(): array
    {
        return [
          'success_ok'          => [self::DEFAULT_RESPONSE],
          'success_head'        => [self::DEFAULT_RESPONSE],
          'error_alreadyActive' => [self::DEFAULT_RESPONSE],
          'error_closeError'    => [self::DEFAULT_RESPONSE],
          'error_responseError' => [false]
        ];
    }

    /**
     * @dataProvider clientProvider
     *
     * @param false|string $response
     */
    public function testClient($response)
    {
        $client = $this->createClient(
          $this->createTestSession($response, $this->createInfo([])),
          $this->createOptions([])
        );
        $this->{$this->dataName()}($client);
    }

    protected function success_ok(Client $client)
    {
        $client->init()->exec();

        self::assertEquals(self::DEFAULT_RESPONSE, $client->getResponse());
        self::assertEquals(self::DEFAULT_RESPONSE, $client->getBody());
    }

    protected function success_head(Client $client)
    {
        $client->options[CURLOPT_RETURNTRANSFER] = true;
        $client->options[CURLOPT_HEADER] = true;

        $client->init()->exec();

        self::assertIsArray($client->getHeadAsArray());
        self::assertIsObject($client->getHead());
        self::assertEquals(self::DEFAULT_RESPONSE, $client->getBody());
    }

    protected function error_responseError(Client $client)
    {
        self::expectException(cURLError::class);
        try {
            self::assertEquals(false, $client->isSessionActive());
            $client->init();
            self::assertEquals(true, $client->isSessionActive());
            $client->exec();
        } catch (RuntimeException $exception) {
            self::assertEquals(false, $client->isSessionActive());
            throw $exception;
        }
    }

    protected function error_alreadyActive(Client $client)
    {
        self::expectException(RuntimeException::class);
        self::expectExceptionCode(0);
        self::expectExceptionMessage('cURL session is already active.');

        $client->init()->init();
    }

    protected function error_closeError(Client $client)
    {
        self::expectException(RuntimeException::class);
        self::expectExceptionCode(0);
        self::expectExceptionMessage('There is no active cURL session to close.');

        $client->close();
    }

    public function testErrorWhileInit()
    {
        $session = $this->createMock(Session::class);
        $session->method('isActive')->will($this->returnValue(false));

        $client = $this->createClient($session, $this->createOptions([]));

        self::expectException(RuntimeException::class);
        self::expectExceptionCode(0);
        self::expectExceptionMessage('Error while init cURL session.');

        $client->init();
    }

    protected function createClient(SessionInterface $session, Options $options): Client
    {
        return new Client($session, $options);
    }

    protected function createInfo(array $info): Info
    {
        return new Info($info);
    }

    protected function createOptions(array $options)
    {
        return new Options($options);
    }

    protected function createTestSession($response, Info $info): SessionInterface
    {
        return new class ($response, $info) implements SessionInterface {
            public bool $session = false;
            /** @var false|string */
            private $response;
            private Info $info;

            public function __construct($response, Info $info)
            {
                $this->response = $response;
                $this->info = $info;
            }

            public function init(): void { $this->session = true; }

            public function isActive(): bool { return $this->session; }

            public function setOptions(array $options): bool { return true; }

            public function setOption(int $option, $value): bool { return true; }

            public function exec() { return $this->response; }

            public function getInfo(): Info { return $this->info; }

            public function close(): void { $this->session = false; }

            public function getError(): string { return 'an-error'; }

            public function getErrorNo(): int { return 13; }
        };
    }
}