<?php

namespace Test\Vulpes\cURL;

use PHPUnit\Framework\TestCase;
use Vulpes\cURL\Info;

class InfoTest extends TestCase
{
    private const TEST_INFO = ['url' => 'test-url-value'];

    public function testOffsetGet()
    {
        $info = new Info(self::TEST_INFO);

        self::assertEquals(self::TEST_INFO['url'], $info->url);
        self::assertEquals(null, $info->header_size);
    }

    public function testOffsetSet()
    {
        $info = new Info(self::TEST_INFO);

        $info->{'url'} = 'another-test-url-value';
        $info->{'header_size'} = 'test-header_size-value';

        self::assertEquals(self::TEST_INFO['url'], $info->url);
        self::assertEquals(null, $info->header_size);
    }

    public function testOffsetExists()
    {
        $info = new Info(self::TEST_INFO);

        self::assertEquals(isset($info->url), isset(self::TEST_INFO['url']));
        self::assertEquals(isset($info->header_size), isset(self::TEST_INFO['header_size']));
    }

    public function testOffsetUnset()
    {
        $info = new Info(self::TEST_INFO);
        unset($info->url);
        self::assertEquals(isset($info->url), isset(self::TEST_INFO['url']));
    }
}