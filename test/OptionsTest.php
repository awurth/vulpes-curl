<?php

namespace Test\Vulpes\cURL;

use PHPUnit\Framework\TestCase;
use Vulpes\cURL\Options;

class OptionsTest extends TestCase
{
    public function testOffsetGet()
    {
        $options = new Options(Options::DEFAULT_OPTIONS);

        self::assertEquals($options[CURLOPT_RETURNTRANSFER], Options::DEFAULT_OPTIONS[CURLOPT_RETURNTRANSFER]);
        self::assertEquals($options['not-existing-key'], null);
    }

    public function testOffsetSet()
    {
        $options = new Options(Options::DEFAULT_OPTIONS);

        $newValue = 'test-new-value';
        $notExistingKey = 'not-existing-key';

        self::assertNotEquals($newValue, $options[$notExistingKey]);
        self::assertNotEquals($newValue, $options[CURLOPT_RETURNTRANSFER]);

        $options[$notExistingKey] = $newValue;
        $options[CURLOPT_RETURNTRANSFER] = $newValue;

        self::assertEquals($newValue, $options[$notExistingKey]);
        self::assertEquals($newValue, $options[CURLOPT_RETURNTRANSFER]);
    }

    public function testOffsetExists()
    {
        $options = new Options(Options::DEFAULT_OPTIONS);

        $existingKey = CURLOPT_RETURNTRANSFER;
        $notExistingKey = '-not-existing-key';

        self::assertEquals(isset(Options::DEFAULT_OPTIONS[$notExistingKey]), isset($options[$notExistingKey]));
        self::assertEquals(isset(Options::DEFAULT_OPTIONS[$existingKey]), isset($options[$existingKey]));
    }

    public function testOffsetUnset()
    {
        $options = new Options(Options::DEFAULT_OPTIONS);
        self::assertEquals(true, isset($options[CURLOPT_RETURNTRANSFER]));
        unset($options[CURLOPT_RETURNTRANSFER]);
        self::assertEquals(false, isset($options[CURLOPT_RETURNTRANSFER]));
    }

    public function testSetter()
    {
        $options = new Options(Options::DEFAULT_OPTIONS);
        self::assertEquals(false, isset($options[CURLOPT_URL]));
        $options->setUrl('test');
        self::assertEquals(true, isset($options[CURLOPT_URL]));
        self::assertEquals('test', $options[CURLOPT_URL]);
    }
}