<?php
/**
 * @var \Composer\Autoload\ClassLoader $classLoader
 */
$classLoader = require __DIR__ . '/vendor/autoload.php';

$classLoader->addPsr4("Test\\Vulpes\\cURL\\", "test/");