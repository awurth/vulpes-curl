<?php

namespace Vulpes\cURL;

use ArrayAccess;

class Options implements ArrayAccess, OptionsInterface
{
    public const DEFAULT_OPTIONS = [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER         => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_AUTOREFERER    => true,
      CURLOPT_CONNECTTIMEOUT => 120,
      CURLOPT_TIMEOUT        => 120,
      CURLOPT_MAXREDIRS      => 10,
      CURLOPT_POST           => true,
      CURLOPT_POSTFIELDS     => null,
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_VERBOSE        => false
    ];

    public const CUSTOMREQUEST_GET = 'GET';
    public const CUSTOMREQUEST_HEAD = 'HEAD';
    public const CUSTOMREQUEST_DELETE = 'DELETE';
    public const CUSTOMREQUEST_POST = 'POST';
    public const CUSTOMREQUEST_CONNECT = 'CONNECT';

    private array $options;

    public function __construct(array $options = self::DEFAULT_OPTIONS)
    {
        $this->options = $options;
    }

    public function setUrl(string $url): Options
    {
        $this->offsetSet(CURLOPT_URL, $url);
        return $this;
    }

    public function setCustomRequest(string $customRequest): Options
    {
        $this->offsetSet(CURLOPT_CUSTOMREQUEST, $customRequest);
        return $this;
    }

    public function setPostFields($fields): Options
    {
        $this->offsetSet(CURLOPT_POSTFIELDS, $fields);
        return $this;
    }

    public function setHttpHeader(array $header): Options
    {
        $this->offsetSet(CURLOPT_HTTPHEADER, $header);
        return $this;
    }

    public function getHttpHeader(): array
    {
        return $this->offsetGet(CURLOPT_HTTPHEADER) ?: [];
    }

    public function addHttpHeader(string $value): Options
    {
        $header = $this->getHttpHeader();
        $header[] = $value;
        return $this->setHttpHeader($header);
    }

    public function basicAuthorization(string $username, string $password)
    {
        return $this->addHttpHeader("Authorization: Basic " . base64_encode($username . ':' . $password));
    }

    public function bearerAuthorization(string $token)
    {
        return $this->addHttpHeader("Authorization: Bearer " . $token);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->options[$offset] : null;
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->options[$offset]);
        }
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->options);
    }

    public function offsetSet($offset, $value)
    {
        $this->options[$offset] = $value;
    }

    public function toArray(): array
    {
        return $this->options;
    }
}