<?php

namespace Vulpes\cURL;

interface SessionInterface
{
    public function init(): void;

    public function isActive(): bool;

    public function setOptions(array $options): bool;

    public function setOption(int $option, $value): bool;

    /**
     * @return bool|string
     */
    public function exec();

    public function getInfo(): Info;

    public function close(): void;

    public function getError(): string;

    public function getErrorNo(): int;
}