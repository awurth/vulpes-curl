<?php

namespace Vulpes\cURL;

use ArrayObject;

/**
 * @property-read string[]        inline
 * @property-read string|string[] server
 * @property-read string|string[] date
 * @property-read string|string[] content-type
 * @property-read string|string[] cache-control
 */
class Head extends ArrayObject
{
    public const INLINE = 'inline';

    public function __construct(?string $input)
    {
        parent::__construct($this->prepareInput($input), ArrayObject::ARRAY_AS_PROPS);
    }

    public function offsetGet($index)
    {
        return parent::offsetExists($index) ? parent::offsetGet($index) : null;
    }

    public function offsetSet($index, $newValue) { }

    public function offsetUnset($index) { }

    private function prepareInput(?string $input): array
    {
        $data = [self::INLINE => []];

        if (is_null($input)) {
            return $data;
        }

        foreach (explode("\n", $input) as $row) {
            $this->prepareRow($row, $key, $value);
            $this->prepareData($data, $key, $value);
        }

        return $data;
    }

    private function prepareRow(string $row, ?string &$key, ?string &$value): void
    {
        list($key, $value) = array_pad(explode(':', trim($row)), 2, null);

        if (is_null($value)) {
            $value = $key;
            $key = self::INLINE;
        }
    }

    private function prepareData(array &$data, string $key, string $value): void
    {
        if (array_key_exists($key, $data)) {
            if (is_array($data[$key]) === false) {
                $data[$key] = [$data[$key]];
            }
            $data[$key][] = trim($value);
            return;
        }
        $data[$key] = trim($value);
    }
}