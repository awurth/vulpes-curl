<?php

namespace Vulpes\cURL;

/**
 * @property-read null|string url
 * @property-read null|string content_type
 * @property-read null|int    http_code
 * @property-read null|int    header_size
 * @property-read null|int    request_size
 * @property-read null|int    filetime
 * @property-read null|int    ssl_verify_result
 * @property-read null|int    redirect_count
 * @property-read null|float  total_time
 * @property-read null|float  namelookup_time
 * @property-read null|float  connect_time
 * @property-read null|float  pretransfer_time
 * @property-read null|int    size_upload
 * @property-read null|int    size_download
 * @property-read null|int    speed_download
 * @property-read null|int    speed_upload
 * @property-read null|int    download_content_length
 * @property-read null|int    upload_content_length
 * @property-read null|float  starttransfer_time
 * @property-read null|int    redirect_time
 * @property-read null|string redirect_url
 * @property-read null|string primary_ip
 * @property-read null|int    primary_port
 * @property-read null|string local_ip
 * @property-read null|int    local_port
 * @property-read null|int    http_version
 * @property-read null|int    protocol
 * @property-read null|int    ssl_verifyresult
 * @property-read null|string scheme
 */
class Info
{
    private array $storage;

    public function __construct(array $storage)
    {
        $this->storage = $storage;
    }

    public function toArray(): array
    {
        return $this->storage;
    }

    public function __get(string $name)
    {
        return array_key_exists($name, $this->storage) ? $this->storage[$name] : null;
    }

    public function __isset(string $name)
    {
        return array_key_exists($name, $this->storage);
    }

    public function __set(string $name, $value) { }

    public function __unset(string $name) { }
}