<?php

namespace Vulpes\cURL;

class Session implements SessionInterface
{
    /**
     * @var resource|false
     */
    private $session = false;

    public function init(): void
    {
        $this->session = curl_init();
    }

    public function isActive(): bool
    {
        return $this->session !== false;
    }

    public function setOptions(array $options): bool
    {
        return curl_setopt_array($this->session, $options);
    }

    public function setOption(int $option, $value): bool
    {
        return curl_setopt($this->session, $option, $value);
    }

    public function exec()
    {
        return curl_exec($this->session);
    }

    public function getInfo(): Info
    {
        return new Info(curl_getinfo($this->session));
    }

    public function close(): void
    {
        curl_close($this->session);
        $this->session = false;
    }

    public function getError(): string
    {
        return curl_error($this->session);
    }

    public function getErrorNo(): int
    {
        return curl_errno($this->session);
    }
}