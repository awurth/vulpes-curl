<?php

namespace Vulpes\cURL;

use CURLFile;
use Vulpes\cURL\Exception\cURLError;
use Vulpes\cURL\Exception\RuntimeException;
use Vulpes\cURL\Exception\HttpException;

class Client
{
    /**
     * @var string|bool
     */
    private $response = false;
    private SessionInterface $session;

    public OptionsInterface $options;
    public ?Info $info;

    public function __construct(SessionInterface $session, OptionsInterface $options)
    {
        $this->session = $session;
        $this->options = $options;
    }

    /**
     * @return \Vulpes\cURL\Client
     * @throws \Vulpes\cURL\Exception\RuntimeException
     */
    public function init(): Client
    {
        if ($this->session->isActive()) {
            throw new RuntimeException('cURL session is already active.');
        }

        $this->session->init();

        if ($this->session->isActive() === false) {
            throw new RuntimeException('Error while init cURL session.');
        }

        return $this;
    }

    public function isSessionActive(): bool
    {
        return $this->session->isActive();
    }

    /**
     * @return \Vulpes\cURL\Client
     * @throws \Vulpes\cURL\Exception\HttpException
     * @throws \Vulpes\cURL\Exception\cURLError
     */
    public function exec(): Client
    {
        $this->session->setOptions($this->options->toArray());
        $this->response = $this->session->exec();

        if ($this->response === false) {
            $error = $this->createError();
            $this->session->close();
            throw $error;
        }

        $this->info = $this->session->getInfo();

        if ($this->info->http_code < 300) {
            return $this;
        }

        throw new HttpException($this->info->http_code);
    }

    /**
     * @return \Vulpes\cURL\Client
     * @throws \Vulpes\cURL\Exception\RuntimeException
     */
    public function close(): Client
    {
        if ($this->session->isActive()) {
            $this->session->close();
            return $this;
        }
        throw new RuntimeException('There is no active cURL session to close.');
    }

    public function createCURLFile(string $fileName, string $mimeType = null, string $postName = null): CURLFile
    {
        return new CURLFile($fileName, $mimeType, $postName);
    }

    public function getResponse() { return $this->response; }

    /**
     * @return \Vulpes\cURL\Head
     * @throws \Vulpes\cURL\Exception\RuntimeException
     */
    public function getHead(): Head
    {
        return new Head($this->getHeadAsString());
    }

    /**
     * @return string|null
     * @throws \Vulpes\cURL\Exception\RuntimeException
     */
    public function getHeadAsString(): ?string
    {
        if ($this->hasResponseHead()) {
            return trim(substr($this->response, 0, $this->info->header_size));
        }
        throw new RuntimeException('There is no header or return transfer set in request.');
    }

    /**
     * @return array|null
     * @throws \Vulpes\cURL\Exception\RuntimeException
     */
    public function getHeadAsArray(): ?array
    {
        if (is_null($head = $this->getHeadAsString())) {
            return null;
        }
        return array_filter(explode("\n", $head), 'trim');
    }

    public function getBody(): ?string
    {
        if ($this->hasResponseHead()) {
            return substr($this->response, $this->info->header_size);
        }
        return is_string($this->response) ? $this->response : null;
    }

    public function hasResponseHead(): bool
    {
        return $this->options[CURLOPT_RETURNTRANSFER] && $this->options[CURLOPT_HEADER];
    }

    private function createError(): cURLError
    {
        return new cURLError($this->session->getError(), $this->session->getErrorNo());
    }
}