<?php

namespace Vulpes\cURL;

interface OptionsInterface
{
    public function toArray(): array;
}