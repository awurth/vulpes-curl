**A simple cURL client.**

Example
```php
use Vulpes\cURL\Client;
use Vulpes\cURL\Exception\Exception;
use Vulpes\cURL\Options;
use Vulpes\cURL\Session;

$options = new Options;
$options[CURLOPT_HEADER] = true;
$options[CURLOPT_RETURNTRANSFER] = true;

$client = new Client(
  new Session,
  $options
);

$options->setUrl('https://packagist.org/');

try {

    $client->init();
    $client->exec();
    $client->close();
    
    var_dump('http_code: ' . $client->info->http_code);

    print $client->getBody();

} catch (Exception $exception) {

    print $exception->getMessage();

}
```